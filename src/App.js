import './App.css';
import { useState } from 'react';
import ListData from './screens/ListData';
import FormData from './screens/AddData';
import EditData from './screens/EditData';

function App() {
  // State untuk menyimpan data untuk ADD DATA
  const [players, setPlayers] = useState([]);

  // State untuk menyimpan hasil filter dari data
  const [filteredPlayers, setFilteredPlayers] = useState([]);

  // State untuk tampilan layar yang sedang ditampilkan
  const [screen, setScreen] = useState("LIST_DATA");

  // State untuk menyimpan data yang dipilih untuk diedit
  const [selectedPlayer, setSelectedPlayer] = useState(null);

  // fungsi untuk ADD Data
  const submitHandler = (fields) => {
    const newPlayers = [...players];
    if (selectedPlayer) {
      const index = newPlayers.findIndex((item) => item.id === selectedPlayer.id);
      if (index !== -1) {
        newPlayers[index] = { ...newPlayers[index], ...fields };
        setSelectedPlayer(null);
      }
    } else {
      newPlayers.push({ id: newPlayers.length + 1, ...fields });
    }
    setPlayers(newPlayers.sort((a, b) => a.id - b.id));
    setFilteredPlayers(newPlayers);
    setScreen("LIST_DATA");
  };

  // Fungsi untuk mengatur data yang akan diedit
  const editPlayerHandler = (player) => {
    setSelectedPlayer(player);
    setScreen("EDIT_DATA");
  };

  // Fungsi untuk mengatur filter pencarian
  const searchHandler = (searchTerm) => {
    if (searchTerm) {
      const filtered = players.filter((player) => player.username.toLowerCase().includes(searchTerm.toLowerCase()));
      setFilteredPlayers(filtered);
    } else {
      setFilteredPlayers(players);
    }
  };

  if (screen === "LIST_DATA") {
    return (
      <ListData
        data={filteredPlayers}
        onAddPlayer={() => setScreen("ADD_DATA")}
        onEditPlayer={editPlayerHandler}
        onSearch={searchHandler}
      />
    );
  } else if (screen === "ADD_DATA") {
    return <FormData onBack={() => setScreen("LIST_DATA")} onSubmit={submitHandler} />;
  } else if (screen === "EDIT_DATA") {
    return (
      <EditData
        data={selectedPlayer}
        onBack={() => setScreen("LIST_DATA")}
        onSubmit={submitHandler}
      />
    );
  }
}

export default App;
