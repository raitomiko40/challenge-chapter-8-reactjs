import React, { useState } from 'react';

export default function EditData(props) {
    const [fields, setFields] = useState(props.data);

    const fieldChangeHandler = (field, value) => {
      setFields((prevFields) => ({
        ...prevFields,
        [field]: value,
      }));
    };
  
    const submitHandler = () => {
      props.onSubmit(fields);
    };
  
    return (
      <div className="container form-box">
        <h2>Edit Data</h2>
        <div className="item-box mb-3" >
          <label className="form-label ">
            Username
          </label>
          <input
            type="text"
            className="form-control"
            value={fields.username}
            onChange={(e) => fieldChangeHandler('username', e.target.value)}
          />
        </div>
        <div className="item-box mb-3">
          <label className="form-label">
            Email
          </label>
          <input
            type="text"
            className="form-control"
            value={fields.email}
            onChange={(e) => fieldChangeHandler('email', e.target.value)}
          />
        </div>
        <div className="item-box mb-3">
          <label className="form-label">
            Experience
          </label>
          <input
            type="text"
            className="form-control"
            value={fields.experience}
            onChange={(e) => fieldChangeHandler('experience', e.target.value)}
          />
        </div>
        <div className="item-box mb-3">
          <label className="form-label">
            Level
          </label>
          <input
            type="text"
            className="form-control"
            value={fields.level}
            onChange={(e) => fieldChangeHandler('level', e.target.value)}
          />
        </div>
        <button type="button" className="btn btn-back" onClick={props.onBack}>
        Back To List Data
        </button>
        <button type="button" className="btn btn-submit ms-4" onClick={submitHandler}>
          Submit
        </button>
      </div>
    );
  }
  
  
  