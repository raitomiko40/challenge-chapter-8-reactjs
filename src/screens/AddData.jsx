import React, { useState } from "react"

export default function AddData(props){
    const [fields, setFields] = useState({
        username: "",
        email: "",
        experience: "",
        level: "",
    })

  
    const fieldChangeHandler = (field, value) => {
        const newFields = {...fields}
        newFields[field] = value
        setFields(newFields)
    }
    return (
        <div className="container form-box">
        <h2>Add Data</h2>
             <div className="item-box mb-3">
                 <label className="form-label" >Username</label>
                <input 
                    type="text"
                    className="form-control"
                    value={fields.username}
                    onChange={(e)=> fieldChangeHandler("username", e.target.value)}    
                />
            </div>
            <div className="item-box  mb-3">
                <label className="form-label" >Email</label>
                <input 
                    type="text" 
                    className="form-control"
                    value={fields.email}
                    onChange={(e)=> fieldChangeHandler("email", e.target.value)} 
                />
            </div>
            <div className="item-box mb-3">
                <label className="form-label" >Experience</label>
                <input 
                     type="text" 
                     className="form-control"
                     value={fields.experience}
                     onChange={(e)=> fieldChangeHandler("experience", e.target.value)}
                />
            </div>
            <div className="item-box mb-3">
                <label className="form-label" >Level</label>
                <input 
                     type="text" 
                     className="form-control"
                     value={fields.level}
                     onChange={(e)=> fieldChangeHandler("level", e.target.value)}
                />
            </div>
            <button
                type="submit" 
                className="btn btn-back " 
                onClick={props.onBack}>Back To List Data</button>
            <button 
                type="submit" 
                className="btn btn-submit ms-4"
                onClick={() => props.onSubmit(fields)} >Submit</button>    
    </div>
    )
}