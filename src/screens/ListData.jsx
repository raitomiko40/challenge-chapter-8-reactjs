import React, { useState } from 'react';

export default function ListData(props) {
  const [searchTerm, setSearchTerm] = useState('');

  // Fungsi untuk menangani pencarian
  const handleSearch = () => {
    props.onSearch(searchTerm);
  };

  return (
    <div className="container">
      <div className="input-group">
        <div className="form-outline mt-3">
          <input
            placeholder="Search by username"
            type="search"
            id="form1"
            className="form-control"
            onChange={(e) => setSearchTerm(e.target.value)}
          />
        </div>
        <button className="btn btn-search mt-3 ms-4" onClick={handleSearch}>
          Search
        </button>
        <button type="button" className="btn btn-add mt-3 position-absolute top-0 end-0 me-4" onClick={props.onAddPlayer}>
          Add Data
        </button>
      </div>
      <table className="table mt-3">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Username</th>
            <th scope="col">Email</th>
            <th scope="col">Experience</th>
            <th scope="col">Level</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          {props.data.map((item, index) => (
            <tr key={item.id}>
              <th scope="row">{index + 1}</th>
              <td>{item.username}</td>
              <td>{item.email}</td>
              <td>{item.experience}</td>
              <td>{item.level}</td>
              <td>
                <button type="button" className="btn btn-edit" onClick={() => props.onEditPlayer(item)}>
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
